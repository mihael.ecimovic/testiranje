# metode i tehnike testiranja programske podrške

Cilj ovog projekta je testiranje različitih značajki na Wikipediji koristeći Selenium i TestNG.

## Pregled

Projekt sadrži automatizirane testove napisane u Javi koristeći Selenium WebDriver i TestNG okvir. Ovi testovi su dizajnirani za provjeru različitih funkcionalnosti na web stranici Wikipedije.

## Testni slučajevi

1. **verifyPageTitle**: Provjerava odgovara li naslov stranice na Wikipediji očekivanom naslovu.
2. **verifySearchBox**: Osigurava da je polje za pretraživanje prikazano na početnoj stranici Wikipedije.
3. **verifyLanguageButton**: Provjerava prisutnost gumba za jezik na početnoj stranici Wikipedije.
4. **verifyLicenseText**: Provjerava je test za licencu prikazan na početnoj stranici Wikipedije.
5. **verifyFooterLinks**: Testira jesu li poveznice na druge Wikimedijine projekte ispravno prikazane.

## Struktura Projekta

- **src/test/java/MyTest**: Najbitniji folder, sarži testove.
- **pom.xml**: Maven konfiguracijska datoteka projekta koja sadrži ovisnosti i postavke izgradnje.
- **testng.xml**: TestNG konfiguracijska datoteka koja specificira testni skup i testne razrede za pokretanje.

## Početak

1. Klonirajte ovaj repozitorij na svoje lokalno računalo.
2. Postavite ovisnosti o Selenium WebDriveru i TestNG-u u vašem Maven projektu.
3. Ako je potrebno, ažurirajte putanju izvršne datoteke WebDrivera u testnom kodu.
4. Pokrenite testove.

## Ovisnosti

- Java
- Maven
- Selenium WebDriver
- TestNG
