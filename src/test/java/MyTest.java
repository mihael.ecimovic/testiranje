import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import java.util.List;
import java.util.Arrays;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MyTest {
    public WebDriver driver;
    public String testURL = "https://www.wikipedia.org";

    @BeforeMethod
    public void setupTest() {
        System.setProperty("webdriver.chrome.driver", "C:\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get(testURL);
    }

    @Test(priority = 1)
    public void verifyPageTitle() {
        String expectedTitle = "Wikipedia";
        String actualTitle = driver.getTitle();
        Assert.assertEquals(actualTitle, expectedTitle, "Page title is not as expected");
    }

    @Test(priority = 2)
    public void verifySearchBox() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement searchBox = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("searchInput")));
        Assert.assertTrue(searchBox.isDisplayed(), "Search box is not displayed");
    }

    @Test(priority = 3)
    public void verifyLanguageButton() {
        WebElement langButton = driver.findElement(By.id("js-lang-list-button"));
        Assert.assertTrue(langButton.isDisplayed(), "English language button is not displayed");
    }

    @Test(priority = 4)
    public void verifyLicenseText() {
        WebElement licenseText = driver.findElement(By.cssSelector(".site-license"));
        Assert.assertTrue(licenseText.isDisplayed(), "License text is not displayed");
    }

    @Test(priority = 5)
    public void verifyFooterLinks() {
        WebElement footer = driver.findElement(By.cssSelector("footer.footer"));
        Assert.assertTrue(footer.isDisplayed(), "Footer is not displayed");

        List<WebElement> footerLinks = footer.findElements(By.tagName("a"));
        List<String> expectedLinks = Arrays.asList("Commons", "Wikivoyage", "Wiktionary", "Wikibooks", "Wikinews");

        for (WebElement link : footerLinks) {
            String linkText = link.getText().trim();
            if (expectedLinks.contains(linkText)) {
                Assert.assertTrue(link.isDisplayed(), linkText + " link is not displayed");
            }
        }
    }

    @AfterMethod
    public void teardownTest() {
        driver.quit();
    }
}
